import { reactive, onMounted,ref } from 'vue'
export function useTable(loadDataFunc: Function,queryForm: {}) {
    let loading = ref(true)
    let tableData = ref()
    let total = ref(0)
    const pagination = reactive({
        pageNum: 1,
        pageSize: 10
    })
    const getTableData = async () => {
        loading.value = true;
        const res = await loadDataFunc({...queryForm,...pagination})
        tableData.value = res.data.list;
        total.value = res.data.total
        loading.value = false;
    }
    onMounted(() => {
        getTableData()
    })
    // 搜索
    const handleSearch = () => {
        pagination.pageNum = 1
        getTableData()
    }
    const refreshTableInfo = () => {
        pagination.pageNum = 1
        getTableData()
    }
    return {
        loading,
        tableData,
        total,
        pagination,
        getTableData,
        handleSearch,
        refreshTableInfo
    }
}
