
//binding（DirectiveBinding）：一个包含指令信息的对象。（例如：包含指令的名称、值、参数等等）
import {App, DirectiveBinding} from "vue";
export default (app: App) => {
    // 节流 防止按钮多次点击，多次请求
    app.directive("throttle", {
        mounted(el: HTMLElement, binding: DirectiveBinding) {
            const time = binding.value ? binding.value : 2000
            el.timer = null
            el.addEventListener('click', () => {
                //console.log(binding);
                el.disabled = true
                if (el.timer !== null) {
                    clearTimeout(el.timer)
                    el.timer = null
                    el.disabled = true
                }
                el.timer = setTimeout(() => {
                    el.disabled = false
                }, time)
            })
        }
    })
    // 输入框防抖
    app.directive("debounce", {
        mounted(el: HTMLElement, binding: DirectiveBinding) {
            const time = binding.value.time ? binding.value.time : 1000//binding.value.time这是输入框传过来的时间
            const func = binding.value ? binding.value.func : null//binding.value.func 这是输入框传过来的方法
            el.timer = null
            el.addEventListener('input', () => {
                //console.log(binding);
                if (el.timer !== null) {
                    clearTimeout(el.timer)
                    el.timer = null
                }
                el.timer = setTimeout(() => {
                    func && func()
                }, time)
            })
        }
    })
}

