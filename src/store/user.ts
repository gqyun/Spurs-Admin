import {defineStore} from 'pinia'
import {getToken, setToken, removeToken} from '@/utils/storage.ts'
import {login,getIndex} from "@/api/user";


//1.创建store
// store实例相当于一个容器，里面存放的有类似于data，计算属性，方法之类的东西。通过defineStore()方法定义
// 在src下面创建一个store文件夹，再创建与之对应的js文件，比如user.js
export const useUserStore = defineStore('user', {
    id: 'user', // id必填，且需要唯一
    state: () => {
        return {
            token: getToken("Access-Token"), // 登录信息
            // routerList: [], // 路由权限
            init:false,//判断是否获取过路由
            userInfo: null // 用户信息
        }
    },
    actions: {
        userLogin(userInfo) {
            const data = {
                // 'user': JSON.stringify(userInfo),
                'loginName': userInfo.loginName,
                'password': userInfo.password
            }
            return new Promise((resolve, reject) => {
                login(data).then((response) => {
                    const {data} = response
                    this.token = data.accessToken;
                    setToken("Access-Token",data.accessToken)
                    resolve(response)
                }).catch((error) => {
                    resolve(error)
                })
            })
        },
        getInfo(){
            return new Promise((resolve, reject) => {
                getIndex().then(response => {
                    const {data} = response
                    // console.log("222",response);
                    this.userInfo = response.user

                    resolve(data)
                }).catch(error => {
                    console.log(error);
                    this.init = false
                    reject(error)
                })
            })
        },
        logout() {
            return new Promise((resolve) => {
                removeToken("Access-Token")
                this.token = ''
                this.userInfo = null;
                this.init = false
                resolve()
            })
        },
        resetToken() {
            return new Promise((resolve) => {
                this.token = ''
                this.userInfo = null
                removeToken()
                resolve()
            })
        }
    }
})
// import { defineStore } from 'pinia'
// import { getToken, setToken, removeToken } from '@/utils/storage'
// //1.创建store
// // store实例相当于一个容器，里面存放的有类似于data，计算属性，方法之类的东西。通过defineStore()方法定义
// // 在src下面创建一个store文件夹，再创建与之对应的js文件，比如user.js
// console.log(defineStore);
// export const useUserStore = defineStore({
//     id: 'user', // id必填，且需要唯一
//     state: () => {
//         return {
//             name: '以和为贵',
//             age: 20
//         }
//     },
//     actions: {
//         updateName(name: string) {
//             this.name = name
//         },
//         updateAge(age: number) {
//             this.age = age
//         }
//     }
// })
